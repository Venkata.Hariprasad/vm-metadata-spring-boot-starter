# Middleware metadata spring boot starter

This library provides code to facilitate usage of metadata viz., dbmetadata, identity metadata and icoms in microservices using Java.

dbmetadata and identity metadata are injected into pod as a volume mount. This data is automatically synced whenever the secrets are modified.  

The secrets itself are published to GCP via gitlab ci/cd pipelines.

shared db metadata : https://gitlab.com/virginmediaO2/cio-middleware/msa/deployment-configurations/shared-configurations/shared-database-secrets

shared identity metadata : https://gitlab.com/virginmediaO2/cio-middleware/msa/deployment-configurations/shared-configurations/shared-identity-secrets

This library provides a way to read secrets from pod and transforms into datasource and identity objects.

## DBMETADATA Configuration

### Enable for your service:
To enable dbmetadata for your service, use below config :

Environment variables
METADATA_DB_FROM_VAULT=enabled
METADATA_DB_FROM_VAULT_AUTO_REFRESHER=<enabled/disabled>  (when enabled, the db metadata is auto refreshed when secret file is modified)
METADATA_DB_FROM_VAULT_FILENAME=<file name> (name of the secret file used in the deployment.yaml)
<CACHE-MWCONFIG_DATASOURCE>=<data>  (datasource configuration specific to your service)

and add below properties to your application.properties file:
```
vm.metadata.db.fromVault=${METADATA_DB_FROM_VAULT:disabled}
vm.metadata.db.fromVault.autoRefresher=${METADATA_DB_FROM_VAULT_AUTO_REFRESHER:disabled}
vm.metadata.db.fromVault.dirPath=///vault//secrets//db//
vm.metadata.db.fromVault.fileName=${METADATA_DB_FROM_VAULT_FILENAME}
vm.metadata.db.resourceData=${CACHE-MWCONFIG_DATASOURCE}
```

### Refresh db meta data

We can enable auto refresh by providing environment variable DB_METADATA_VAULT_AUTOREFRESHER with value as 'enabled'. 
another way to refresh is by invoking GET refresh/dbmetadata API

## IDENTITY METADATA Configuration

### Enable for your service
To enable identity metadata for your service, use below config :

Environment variables
METADATA_IDENTITY_FROM_VAULT=enabled
METADATA_IDENTITY_FROM_VAULT_AUTO_REFRESHER=<enabled/disabled>  (when enabled, the identity metadata is auto refreshed when secret file is modified)
METADATA_IDENTITY_FROM_VAULT_FILENAME=<file name> (name of the secret file used in the deployment.yaml)

and add below properties to your application.properties file:
```
vm.metadata.identity.fromVault=${DMETADATA_IDENTITY_FROM_VAULT:disabled}
vm.metadata.identity.fromVault.autoRefresher=${METADATA_IDENTITY_FROM_VAULT_AUTO_REFRESHER:disabled}
vm.metadata.identity.fromVault.dirPath=///vault//secrets//identity//
vm.metadata.identity.fromVault.fileName=${METADATA_IDENTITY_FROM_VAULT_FILENAME}
```

### Refresh identity meta data

We can enable auto refresh by providing environment variable METADATA_DB_FROM_VAULT_AUTO_REFRESHER with value as 'enabled'.
another way to refresh is by invoking GET refresh/identitymetadata API


## Maven Dependency
Include this maven dependency in the service level POM file to use this VM core lib

```xml
<dependency>
	<groupId>uk.co.virginmedia.mw.msa.core</groupId>
	<artifactId>vm-metadata-spring-boot-starter</artifactId>
	<version>VERSION</version>
</dependency>
```

## TODOs
- Implement icoms functionality
- Create spring boot datasource from dbmetadata
- Upgrade to spring 3
