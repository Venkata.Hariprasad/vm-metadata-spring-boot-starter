package uk.co.virginmedia.mw.msa.core.metadata;

import java.util.List;

public interface MetadataHolder {
    void refreshMetadata();

    List<String> getDirectoryWatchList();

    boolean matchesWatchingFile(String fileName);
}
