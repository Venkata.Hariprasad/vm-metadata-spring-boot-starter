package uk.co.virginmedia.mw.msa.core.metadata;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;

public class MetadataFileWatcher implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MetadataFileWatcher.class);
    private final WatchService watchService;
    private final List<MetadataHolder> metadataHolders;


    public MetadataFileWatcher(List<MetadataHolder> metadataHolders) throws IOException {
        this.metadataHolders = metadataHolders;
        this.watchService = FileSystems.getDefault().newWatchService();
        metadataHolders.stream().flatMap(e -> e.getDirectoryWatchList().stream()).map(Paths::get).forEach(e -> {
            try {
                e.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
            } catch (IOException ex) {
                LOGGER.error("Error while initializing the watcher");
            }
        });
    }

    @Override
    public void run() {
        while (true) {
            WatchKey key;
            try {
                key = watchService.take();
            } catch (InterruptedException e) {
                return;
            }

            for (WatchEvent<?> event : key.pollEvents()) {
                WatchEvent.Kind<?> kind = event.kind();

                if (kind == StandardWatchEventKinds.OVERFLOW) {
                    continue;
                }

                WatchEvent<Path> ev = (WatchEvent<Path>) event;
                Path fileName = ev.context();

                LOGGER.info("METADATA file event triggered - " + kind + ": " + fileName);
                handleFileModifyEvent(kind, fileName);
            }

            boolean valid = key.reset();
            if (!valid) {
                break;
            }
        }
    }

    private void handleFileModifyEvent(WatchEvent.Kind<?> kind, Path fileName) {
        if (kind == StandardWatchEventKinds.ENTRY_MODIFY) {
            // handle file modification
            LOGGER.info("METADATA file change event is logged for {}-file - {} ", kind, fileName);

            this.metadataHolders.stream()
                    .filter(holder -> holder.matchesWatchingFile(fileName.toFile().getName()))
                    .findFirst()
                    .ifPresent(MetadataHolder::refreshMetadata);
            LOGGER.info("METADATA refresh endpoint is triggered for {}-file - {}", kind, fileName);
        }
    }

    public void init() {
        LOGGER.info("Metadata watcher initialized");
        new Thread(this).start();
        LOGGER.info("Started file watcher");
    }
}
