package uk.co.virginmedia.mw.msa.core.metadata.identity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.virginmedia.mw.msa.core.metadata.ConfigUtil;
import uk.co.virginmedia.mw.msa.core.metadata.MetadataHolder;
import uk.co.virginmedia.mw.msa.core.metadata.identity.dto.IdentityKey;
import uk.co.virginmedia.mw.msa.core.metadata.identity.dto.IdentityMetadata;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;


public class IdentityMetadataHolder implements MetadataHolder {

    private static final Logger LOGGER = LoggerFactory.getLogger(IdentityMetadataHolder.class);
    private final AtomicReference<Map<IdentityKey, IdentityMetadata>> identityData = new AtomicReference<>();
    private final String vaultSecretsDir;
    private final String fileName;

    public IdentityMetadataHolder(String vaultSecretsDir, String fileNames) {
        this.vaultSecretsDir = vaultSecretsDir;
        this.fileName = fileNames;
        loadIdentityData();
    }

    public Optional<IdentityMetadata> get(final String system) {
        return get(system, null, null);
    }

    public Optional<IdentityMetadata> get(final String system, final String testPlatform, final String appId) {
        IdentityKey identityKey = new IdentityKey(system, testPlatform, appId);
        Optional<IdentityMetadata> optionalIdentityMetadata = identityData.get().entrySet().stream()
                .filter(entry -> identityKey.equals(entry.getKey()))
                .map(Map.Entry::getValue)
                .findAny();
        optionalIdentityMetadata.ifPresentOrElse(v -> LOGGER.debug("identityMetadata found for {}", identityKey), () -> LOGGER.error("no identityMetadata found for {}", identityKey));
        return optionalIdentityMetadata;
    }

    private void loadIdentityData() {
        identityData.set(fetchIdentityMetadata());

    }


    private Map<IdentityKey, IdentityMetadata> fetchIdentityMetadata() {
        String vaultSecretPath = String.join("", vaultSecretsDir, fileName);
        return ConfigUtil.fetchVaultData(vaultSecretPath, IdentityMetadata.class);
    }

    @Override
    public void refreshMetadata() {
        loadIdentityData();
    }

    @Override
    public List<String> getDirectoryWatchList() {
        return List.of(vaultSecretsDir);
    }

    @Override
    public boolean matchesWatchingFile(String fileName) {
        return this.fileName.equals(fileName);
    }
}
