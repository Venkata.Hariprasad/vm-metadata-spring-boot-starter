package uk.co.virginmedia.mw.msa.core.metadata.dbmetadata;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.virginmedia.mw.msa.core.metadata.ConfigUtil;
import uk.co.virginmedia.mw.msa.core.metadata.MetadataHolder;
import uk.co.virginmedia.mw.msa.core.metadata.dbmetadata.dto.DBMetadata;
import uk.co.virginmedia.mw.msa.core.metadata.dbmetadata.dto.DataSourceKey;
import uk.co.virginmedia.mw.msa.core.metadata.dbmetadata.dto.Resource;

import javax.sql.DataSource;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;


public class DbMetadataHolder implements MetadataHolder {

    private static final Logger LOGGER = LoggerFactory.getLogger(DbMetadataHolder.class);
    private final AtomicReference<Map<DataSourceKey, DataSource>> dataSourceMap = new AtomicReference<>();
    private final String resourceData;
    private final String vaultSecretDirPath;
    private final String fileName;

    public DbMetadataHolder(String vaultSecretDirPath, String fileName, String resourceData) {
        this.vaultSecretDirPath = vaultSecretDirPath;
        this.fileName = fileName;
        this.resourceData = resourceData;
        loadDataSources();
    }

    /**
     * Gets the data source for the test platform.
     *
     * @param testPlatform the test platform.
     * @return the data source.
     */
    public DataSource get(final String testPlatform) {
        DataSourceKey key = new DataSourceKey(testPlatform, null);
        DataSource dataSource = dataSourceMap.get().get(key);
        if (dataSource == null && testPlatform != null) {
            key = new DataSourceKey(null, null);
            dataSource = dataSourceMap.get().get(key);
        }
        return dataSource;
    }

    /**
     * Gets the data source for the test platform.
     *
     * @param testPlatform the test platform.
     * @param id           the id.
     * @return the data source.
     */
    public DataSource get(final String testPlatform, final BigInteger id) {
        DataSourceKey key = new DataSourceKey(testPlatform, id);
        DataSource dataSource = dataSourceMap.get().get(key);
        if (dataSource == null && testPlatform != null) {
            key = new DataSourceKey(null, id);
            dataSource = dataSourceMap.get().get(key);
        }
        return dataSource;
    }

    private void loadDataSources() {
        final Map<String, DBMetadata> databaseSet = fetchDBMetadata();
        final Map<String, Resource> resourceSet = getData();

        final Set<String> databaseKeys = databaseSet.keySet();
        final Set<String> resourceKeys = resourceSet.keySet();

        // form the intersection of the databaseKeys and resourceKeys
        databaseKeys.retainAll(resourceKeys);

        // there must be at least one pool configured without testPlatform
        boolean ok = false;
        for (final String databaseName : databaseKeys) {
            final Resource resource = resourceSet.get(databaseName);
            if (resource != null && resource.testPlatform() == null) {
                ok = true;
            }
        }
        if (!ok) {
            throw new DataSourceException("there must be a least one pool configured without testPlatform");
        }

        for (final String databaseName : databaseKeys) {
            createPooledDataSource(databaseName, databaseSet, resourceSet);
        }
    }

    private Map<String, Resource> getData() {
        final ObjectMapper mapper = new ObjectMapper();
        final Map<String, Resource> map = new HashMap<>();
        try {
            final CollectionType listType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, Resource.class);
            final List<Resource> values = mapper.readValue(resourceData, listType);
            for (final Resource t : values) {
                final Resource previousValue = map.put(t.name(), t);
                if (previousValue != null) {
                    LOGGER.error("multiple datasource defined with the same name: " + t.name());
                    throw new DataSourceException("multiple datasource defined with the same name: " + t.name());
                }
            }
            return map;
        } catch (final DataSourceException e) {
            throw e;
        } catch (final Exception e) {
            LOGGER.error("unable to parse resourceData", e);
            throw new DataSourceException("unable to parse resourceData", e);
        }
    }

    /**
     * Create pooled data source and add to the data source map
     *
     * @param databaseName the database name
     * @param dataBases    Map<String, DBMetadata>
     * @param resources    Map<String, Resource>
     */
    private void createPooledDataSource(final String databaseName, final Map<String, DBMetadata> dataBases,
                                        final Map<String, Resource> resources) {
        final ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
        try {
            comboPooledDataSource.setDriverClass(dataBases.get(databaseName).driverClass());
        } catch (final Exception e) {
            LOGGER.error("unable to set driver class", e);
        }
        comboPooledDataSource.setDataSourceName(databaseName);
        comboPooledDataSource.setJdbcUrl(dataBases.get(databaseName).jdbcUrl());
        comboPooledDataSource.setUser(dataBases.get(databaseName).user());
        comboPooledDataSource.setPassword(dataBases.get(databaseName).password());
        comboPooledDataSource.setInitialPoolSize(resources.get(databaseName).initialPoolSize());
        comboPooledDataSource.setMinPoolSize(resources.get(databaseName).minPoolSize());
        comboPooledDataSource.setMaxPoolSize(resources.get(databaseName).maxPoolSize());
        comboPooledDataSource.setMaxIdleTime(resources.get(databaseName).maxIdleTime());
        comboPooledDataSource.setAcquireIncrement(resources.get(databaseName).acquireIncrement());
        comboPooledDataSource.setPreferredTestQuery(resources.get(databaseName).preferredTestQuery());
        comboPooledDataSource.setIdleConnectionTestPeriod(resources.get(databaseName).idleConnectionTestPeriod());
        comboPooledDataSource.setCheckoutTimeout(resources.get(databaseName).checkoutTimeout());

        final String testPlatform = resources.get(databaseName).testPlatform();
        final DBMetadata db = dataBases.get(databaseName);
        Map<DataSourceKey, DataSource> dbMap = new HashMap<>();
        if (db.id() == null || db.id().isEmpty()) {
            final DataSourceKey key = new DataSourceKey(testPlatform, null);
            dbMap.put(key, comboPooledDataSource);
        } else {
            for (final BigInteger id : db.id()) {
                final DataSourceKey key = new DataSourceKey(testPlatform, id);
                dbMap.put(key, comboPooledDataSource);
            }
        }
        // update map atomically
        this.dataSourceMap.set(dbMap);
    }


    private Map<String, DBMetadata> fetchDBMetadata() {
        return ConfigUtil.fetchVaultData(String.join("", vaultSecretDirPath, fileName), DBMetadata.class);
    }

    @Override
    public void refreshMetadata() {
        loadDataSources();
    }

    @Override
    public List<String> getDirectoryWatchList() {
        return List.of(vaultSecretDirPath);
    }

    @Override
    public boolean matchesWatchingFile(String fileName) {
        return this.fileName.equals(fileName);
    }
}
