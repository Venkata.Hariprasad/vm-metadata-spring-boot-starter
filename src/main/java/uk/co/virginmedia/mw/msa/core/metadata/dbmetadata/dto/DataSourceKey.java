package uk.co.virginmedia.mw.msa.core.metadata.dbmetadata.dto;

import java.math.BigInteger;

public record DataSourceKey(String testPlatform, BigInteger id) {

}
