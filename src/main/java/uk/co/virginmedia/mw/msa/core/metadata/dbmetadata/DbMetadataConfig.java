package uk.co.virginmedia.mw.msa.core.metadata.dbmetadata;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import uk.co.virginmedia.mw.msa.core.metadata.MetadataFileWatcher;
import uk.co.virginmedia.mw.msa.core.metadata.MetadataHolder;

import java.io.IOException;
import java.util.List;

@RestController
@Configuration
@ConditionalOnProperty(name = "vm.metadata.db.fromVault", havingValue = "enabled")
public class DbMetadataConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(DbMetadataConfig.class);

    @Value("${vm.metadata.db.fromVault.dirPath}")
    private String dbVaultSecretDirPath;

    @Value("${vm.metadata.db.fromVault.fileName}")
    private String fileName;

    @Value("${vm.metadata.db.resourceData}")
    private String dbResourceData;


    @Bean(initMethod = "init")
    @ConditionalOnProperty(name = "vm.metadata.db.autoRefresher", havingValue = "enabled")
    public MetadataFileWatcher fileSystemWatcher(List<MetadataHolder> metadataHolders) {
        try {
            return new MetadataFileWatcher(metadataHolders);
        } catch (IOException e) {
            LOGGER.error("Error occurred during file watcher initialization", e);
        }
        return null;
    }

    @Bean
    DbMetadataHolder dbMetadataHolder() {
        return new DbMetadataHolder(dbVaultSecretDirPath, fileName, dbResourceData);
    }

    @GetMapping(path = "/refresh/dbmetadata")
    public ResponseEntity<String> refreshDatasource() {
        dbMetadataHolder().refreshMetadata();
        return ResponseEntity.status(HttpStatus.OK).body("DB metadata refresh triggered");
    }


}
