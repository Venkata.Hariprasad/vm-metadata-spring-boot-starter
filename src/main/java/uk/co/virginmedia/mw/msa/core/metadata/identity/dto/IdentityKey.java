package uk.co.virginmedia.mw.msa.core.metadata.identity.dto;


import java.util.Objects;


public record IdentityKey(String system, String testPlatform, String appId) {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdentityKey identityKey = (IdentityKey) o;
        if (!identityKey.system.equals(system)) return false;
        if (appId != null && appId.equals(identityKey.appId)) return false;
        return identityKey.testPlatform == null || identityKey.testPlatform.equals(testPlatform);
    }

    @Override
    public int hashCode() {
        return Objects.hash(system, appId, testPlatform);
    }
}
