package uk.co.virginmedia.mw.msa.core.metadata.identity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Configuration
@ConditionalOnProperty(name = "vm.metadata.identity.fromVault", havingValue = "enabled")
public class IdentityMetadataConfig {

    @Value("${vm.metadata.identity.fromVault.dirPath}")
    private String identityVaultSecretDirPath;

    @Value("${vm.metadata.identity.fromVault.filePath}")
    private String identityVaultSecretPath;


    @Bean
    IdentityMetadataHolder identityMetadataHolder() {
        return new IdentityMetadataHolder(identityVaultSecretDirPath, identityVaultSecretPath);
    }

    @GetMapping(path = "/refresh/identitymetadata")
    public ResponseEntity<String> refreshDatasource() {
        identityMetadataHolder().refreshMetadata();
        return ResponseEntity.status(HttpStatus.OK).body("Identity data refresh triggered");
    }


}
