package uk.co.virginmedia.mw.msa.core.metadata.identity.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import uk.co.virginmedia.mw.msa.core.metadata.Mappable;


@JsonIgnoreProperties(ignoreUnknown = true)
public record IdentityMetadata(

        @JsonProperty("testPlatform")
        String testPlatform,
        @JsonProperty("appID")
        String appID,
        @JsonProperty("system")
        String system,
        @JsonProperty("service")
        String service,
        @JsonProperty("user")
        String user,

        @JsonProperty("password")
        String password,

        @JsonProperty("token")
        String token)
        implements Mappable<IdentityKey> {

    @Override
    public IdentityKey getKey() {
        return new IdentityKey(system, testPlatform, appID);
    }
}
