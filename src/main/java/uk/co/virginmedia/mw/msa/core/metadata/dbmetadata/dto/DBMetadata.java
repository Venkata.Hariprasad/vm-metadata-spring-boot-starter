package uk.co.virginmedia.mw.msa.core.metadata.dbmetadata.dto;

import uk.co.virginmedia.mw.msa.core.metadata.Mappable;

import java.math.BigInteger;
import java.util.List;


public record DBMetadata(String name, String user, String password, String jdbcUrl, String driverClass,
                         List<BigInteger> id) implements Mappable<String> {
    @Override
    public String getKey() {
        return name;
    }
}
