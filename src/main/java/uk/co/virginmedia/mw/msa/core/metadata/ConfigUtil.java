package uk.co.virginmedia.mw.msa.core.metadata;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ConfigUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigUtil.class);

    public static <T extends Mappable<K>, K> Map<K, T> fetchVaultData(String vaultSecretPath, Class<T> clazzType) {
        File secretFile = new File(vaultSecretPath);
        if (secretFile.exists()) {
            try (FileInputStream secretFileInputStream = new FileInputStream(secretFile)) {
                ObjectMapper objectMapper = new ObjectMapper();
                final CollectionType listType = objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, clazzType);
                final List<T> values = objectMapper.readValue(secretFileInputStream, listType);
                LOGGER.info("Successfully loaded {} entries from vault secret file {}.", values.size(), vaultSecretPath);
                return values.stream().collect(Collectors.toMap(Mappable::getKey, el -> el));
            } catch (Exception exp) {
                LOGGER.error("Failed to load vault data from secret file {}", vaultSecretPath, exp);
            }
        }
        return Collections.emptyMap();
    }
}
