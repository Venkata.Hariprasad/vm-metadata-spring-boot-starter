package uk.co.virginmedia.mw.msa.core.metadata.dbmetadata.dto;

import com.fasterxml.jackson.annotation.JsonProperty;


public record Resource(
        @JsonProperty("name")
        String name,

        @JsonProperty("testPlatform")
        String testPlatform,

        @JsonProperty("minPoolSize")
        int minPoolSize,

        @JsonProperty("maxPoolSize")
        int maxPoolSize,

        @JsonProperty("initialPoolSize")
        int initialPoolSize,

        @JsonProperty("acquireIncrement")
        int acquireIncrement,

        @JsonProperty("maxIdleTime")
        int maxIdleTime,

        @JsonProperty("preferredTestQuery")
        String preferredTestQuery,

        @JsonProperty("idleConnectionTestPeriod")
        int idleConnectionTestPeriod,

        @JsonProperty("checkoutTimeout")
        int checkoutTimeout
) {
}
