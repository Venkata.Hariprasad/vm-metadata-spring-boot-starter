package uk.co.virginmedia.mw.msa.core.metadata;

public interface Mappable<T> {
    T getKey();
}
