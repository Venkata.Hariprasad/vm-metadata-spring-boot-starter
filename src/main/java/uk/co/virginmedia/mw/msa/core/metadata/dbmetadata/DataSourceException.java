package uk.co.virginmedia.mw.msa.core.metadata.dbmetadata;

/**
 * Runtime exception to give optional exception handling for data sources.
 */
public class DataSourceException extends RuntimeException {

    /**
     * Constructs the DataSourceException.
     *
     * @param message the message.
     * @param cause   the cause.
     */
    public DataSourceException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs the DataSourceException.
     *
     * @param message the message.
     */
    public DataSourceException(final String message) {
        super(message);
    }

}
