package uk.co.virginmedia.mw.msa.core.metadata;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import uk.co.virginmedia.mw.msa.core.metadata.dbmetadata.DataSourceException;
import uk.co.virginmedia.mw.msa.core.metadata.dbmetadata.DbMetadataHolder;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

class DBMetadataFileIntegrationTest {

    private final static String resourceData = """
            [
            {
              "name": "TEST",
               "minPoolSize": 1,
               "maxPoolSize": 2,
               "initialPoolSize": 1,
               "acquireIncrement": 1,
               "maxIdleTime": 3600,
               "preferredTestQuery": "select current date from sysibm.sysdummy1",
               "idleConnectionTestPeriod": 120,
                "checkoutTimeout": 3000
            }
            ]            
            """;
    private final static String dbMetadataInitial = """
            [
               {
                   "name": "TEST",
                   "user": "username",
                   "password": "password",
                   "jdbcUrl": "url",
                   "driverClass": "oracle.jdbc.driver.OracleDriver"
                 }
               ]          
            """;
    private final static String dbMetadataUpdated = """
            [
               {
                   "name": "TEST",
                   "user": "username2",
                   "password": "password",
                   "jdbcUrl": "url",
                   "driverClass": "oracle.jdbc.driver.OracleDriver"
                 },
                 {
                   "name": "TEST_A",
                   "user": "username2",
                   "password": "password",
                   "jdbcUrl": "url",
                   "driverClass": "oracle.jdbc.driver.OracleDriver"
                 }
               ]          
            """;
    MetadataFileWatcher metadataFileWatcher;

    @AfterAll
    static void afterAll() throws IOException {
        writeToFile(dbMetadataInitial);
    }

    private static void writeToFile(String str) throws IOException {
        Path path = Paths.get("src/test/resources/db.json");
        try (BufferedWriter bw = Files.newBufferedWriter(path, Charset.defaultCharset())) {
            bw.write(str);
        }
    }

    @Test
    void init_valid_db_secretfile() throws IOException {
        DbMetadataHolder metadataHolder = new DbMetadataHolder("src/test/resources/", "db.json", resourceData);
        metadataFileWatcher = new MetadataFileWatcher(List.of(metadataHolder));
        metadataFileWatcher.init();
        var result = metadataHolder.get(null);
        assertNotNull(result);
    }

    @Test
    void init_invalid_db_secretfile() throws IOException {
        DbMetadataHolder metadataHolder = new DbMetadataHolder("src/test/resources/", "test-invalidDriver.json", resourceData);
        metadataFileWatcher = new MetadataFileWatcher(List.of(metadataHolder));
        metadataFileWatcher.init();
        var result = metadataHolder.get(null);
        assertNotNull(result);
    }

    @Test
    void init_empty_db_secretfile() {
        assertThrowsExactly(DataSourceException.class, ()
                -> new DbMetadataHolder("src/test/resources/", "test-empty.json", resourceData));
    }

    @Test
    void fileChange_modify_event() throws IOException {
        DbMetadataHolder metadataHolder = new DbMetadataHolder("src/test/resources/", "db.json", resourceData);
        metadataFileWatcher = new MetadataFileWatcher(List.of(metadataHolder));
        metadataFileWatcher.init();
        var result = metadataHolder.get(null);
        assertNotNull(result);
        writeToFile(dbMetadataUpdated);
        var resultUpdated = metadataHolder.get("A");
        assertNotNull(resultUpdated);

    }
}
