package uk.co.virginmedia.mw.msa.core.metadata;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import uk.co.virginmedia.mw.msa.core.metadata.identity.IdentityMetadataHolder;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.awaitility.Awaitility.await;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertTrue;

class IdentityMetadataFileIntegrationTest {

    private final static String identityMetadataInitial = """
            [
               {
                   "system": "TEST",
                   "appID":"APP",
                   "user": "username",
                   "password": "password"
                 }
            ]        
            """;
    private final static String identityMetadataUpdated = """
            [
                             {
                                 "system": "TEST",
                                 "appID":"APP",
                                 "user": "username",
                                 "password": "password"
                               },
                               {
                                 "system": "TEST",
                                 "appID":"APP2",
                                 "user": "username",
                                 "password": "password"
                               }
                          ]
            """;
    MetadataFileWatcher metadataFileWatcher;

    @AfterAll
    static void afterAll() throws IOException {
        writeToFile(identityMetadataInitial);
    }

    private static void writeToFile(String str) throws IOException {
        Path path = Paths.get("src/test/resources/identity.json");
        try (BufferedWriter bw = Files.newBufferedWriter(path, Charset.defaultCharset())) {
            bw.write(str);
        }
    }

    @Test
    void init() throws IOException {
        IdentityMetadataHolder metadataHolder = new IdentityMetadataHolder("src/test/resources/", "identity.json");
        metadataFileWatcher = new MetadataFileWatcher(List.of(metadataHolder));
        metadataFileWatcher.init();
        var result = metadataHolder.get("TEST");
        assertTrue(result.isPresent());
    }

    @Test
    void fileChange() throws IOException {
        IdentityMetadataHolder metadataHolder = new IdentityMetadataHolder("src/test/resources/", "identity.json");
        metadataFileWatcher = new MetadataFileWatcher(List.of(metadataHolder));
        metadataFileWatcher.init();
        var result = metadataHolder.get("TEST");
        assertTrue(result.isPresent());
        writeToFile(identityMetadataUpdated);

        await().until(() -> metadataHolder.get("TEST", null, "APP2").isPresent(), equalTo(true));

    }
}
