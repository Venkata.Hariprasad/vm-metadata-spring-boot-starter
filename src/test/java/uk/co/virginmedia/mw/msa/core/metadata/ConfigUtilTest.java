package uk.co.virginmedia.mw.msa.core.metadata;

import org.junit.jupiter.api.Test;
import uk.co.virginmedia.mw.msa.core.metadata.dbmetadata.dto.DBMetadata;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ConfigUtilTest {

    @Test
    void fetchVaultData() {
        var result = ConfigUtil.fetchVaultData("src/test/resources/test.json", DBMetadata.class);
        assertNotNull(result);
        assertFalse(result.isEmpty());
    }

    @Test
    void fetchVaultData_file_does_not_exist() {
        var result = ConfigUtil.fetchVaultData("src/test/resources/unknown.json", DBMetadata.class);
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    void fetchVaultData_invalid_json() {
        var result = ConfigUtil.fetchVaultData("src/test/resources/test-invalid.json", DBMetadata.class);
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }
}
